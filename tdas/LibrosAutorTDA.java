package tdas;

public interface LibrosAutorTDA
{	
	/**
	 * inicializa la estructura
	 * PRE:
	 * POS: estructura inicializada
	 */
	public void inicializar();
	
	/**
	 * Devuelve titulo del libro
	 * PRE: inicializada y no vacia
	 * POS: -
	 * @return titulo
	 */
	public String titulo();
	
	/**
	 * Devuelve el precio del libro
	 * PRE: inicializado y no vacia
	 * POS: -
	 * @return precio
	 */
	public double precio();
	
	/**
	 * Indica si la estructura esta vacia (true) o no (false)
	 * PRE: inicializado
	 * POS:
	 * @return
	 */
	public boolean estaVacio();
	
	/**
	 * Devuelve el hijo izquierdo
	 * PRE: inicializado y no vacio
	 * @return LibrosAutorTDA
	 */
	public LibrosAutorTDA hijoIzquierdo();
	
	/**
	 * Devuelve el hijo derecho
	 * PRE: inicializado y no vacio
	 * @return LibrosAutorTDA
	 */
	public LibrosAutorTDA hijoDerecho();
		
	/**
	 * agrega precio y titulo, si el precio existe no hace nada
	 * PRE: inicializada
	 * POS: si el precio no existe, la estructura tiene un titulo precio m�s
	 * @param titulo
	 * @param precio
	 */
	public void agregar(String titulo, double precio);
}
