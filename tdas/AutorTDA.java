package tdas;

public interface AutorTDA
{
	/**
	 * inicializa la estructura
	 * PRE:-
	 * POS:-
	 */
	public void inicializar();
	
	
	/**
	 * Devuelve nombre de autor
	 * PRE: inicializado y no vacio
	 * POS: -
	 * @return autor
	 */
	public String autor();
	
	/**
	 * Devuelve los libros del autor
	 * PRE: inicializado y no vacio
	 * POS: -
	 * @return LibrosAutorTDA
	 */
	public LibrosAutorTDA libros();
	
	
	/**
	 * indice si el arbol esta vacio
	 * PRE: inicializado
	 * POS: -
	 * @return boolean
	 */
	public boolean estaVacio();
	
	/**
	 * devuelve el hijo izquierdo
	 * PRE: inicializado y no vacio
	 * POS: -
	 * @return AutorTDA
	 */
	public AutorTDA hijoIzquierdo();
	
	/**
	 * devuelve el hijo derecho
	 * PRE: inicializado y no vacio
	 * POS: -
	 * @return AutorTDA
	 */
	public AutorTDA hijoDerecho();
	
	/**
	 * agrega un titulo y su precio a los libros del autor
	 * si el autor no existe tambien lo agrega. Si existe un precio igual al pasado
	 * no hace nada
	 * PRE: inicializado
	 * POS: un titulo m�s si el precio no existe, un autor m�s si el autor no existe
	 * @param autor
	 * @param titulo
	 * @param precio
	 */
	public void agregar(String autor,String titulo,double precio);

	//public void eliminar(int elemento);
}
