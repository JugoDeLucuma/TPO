package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import app.Catalogo;
import implementaciones.Autor;
import tdas.AutorTDA;

public class TestCatalogo {

	@Test
	public void testAutoresCuyoLibroDeMenorPrecioEstaEntreRango() {
		fail("Not yet implemented");
	}

	@Test
	public void testEsteTituloPerteneceAlAutorYTienePrecioMenorAlDado() {
		fail("Not yet implemented");
	}

	@Test
	public void testLibroPerteneceAAutor() 
	{
		AutorTDA autores = new Autor();
		autores.agregar("Borges", "El Aleph", 349.99);
		autores.agregar("Borges", "La memoria de Shakespeare", 300.5);
		autores.agregar("Borges", "El libro de arena", 50.57);
		
		autores.agregar("Cortazar", "Rayuela", 500);
		autores.agregar("Cortazar", "Bestiario",400);
		
		Catalogo catalogo = new Catalogo();
		//assertTrue(catalogo.libroPerteneceAAutor(autores, "Borges", "El Aleph"));
		//assertTrue(catalogo.libroPerteneceAAutor(autores, "Borges", "La memoria de Shakespeare"));
		assertTrue(catalogo.libroPerteneceAAutor(autores, "Borges", "El libro de arena"));
		//assertTrue(catalogo.libroPerteneceAAutor(autores, "Cortazar", "Rayuela"));
		//assertTrue(catalogo.libroPerteneceAAutor(autores, "Cortazar", "Bestiario"));
	}

}
